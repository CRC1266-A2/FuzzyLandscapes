# FuzzyLandscapes

The goal of this package is the:

- Fuzzification of crisp raster data
- Rule-based combination of fuzzy datasets
- Defuzzification of those combined datasets


### Citation

> Hamer, Wolfgang and Knitter, Daniel (2018) FuzzyLandscapes -- Fuzzy analyses with a focus on raster data. Zenodo: doi:10.5281/zenodo.1747005. 


### Installation

You can install FuzzyLandscapes from gitlab with:

```r
if(!require('devtools')) install.packages('devtools')
devtools::install_git('https://gitlab.com/CRC1266-A2/FuzzyLandscapes.git')
```

