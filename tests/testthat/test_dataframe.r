context("Check classes")

test_that("rules is data.frame", {
  rules <- data.frame()
  expect_equal(class(rules), "data.frame")  
})
